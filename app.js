const Koa = require('koa');
const Router = require('koa-router');
 
// 实例化koa
const app = new Koa();
const router = new Router();

// 匹配路由之前做的事
app.use(async(ctx,next)=>{
  console.log(new Date());
  await next(); //当前路由
})

// 路由
router.get('/', async (ctx) => {
  ctx.body = {
    msg: 'Hello Koa Interfaces'
  }
})
router.get('/news', async (ctx) => {
  console.log('这是新闻');
  await next(); //匹配到路由后继续向下匹配
})
router.get('/news', async (ctx) => {
  ctx.body = {
    msg: 'Hello Koa Interfaces'
  }
})
// 配置路由
app
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(3000);
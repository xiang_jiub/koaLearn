const Koa = require('koa');
const Router = require('koa-router');
 
// 实例化koa
const app = new Koa();
const router = new Router();


app.use(async(ctx,next)=>{
    console.log('中间件01');
     await next;    //路由级别中间件
     if(ctx.status == 404){    
         ctx.status=404;
         ctx.body="这是404页面"
     }else{
         console.log(ctx.url);
     }
})
// 路由
router.get('/',async (ctx) => {
  ctx.body = 'Hello Koa Interfaces';
})

router.get('/news', async(ctx) => {
  console.log('这是新闻2');
  ctx.body='这是一个新闻';
})

router.get('/login', async (ctx) => {
    console.log('这登陆页2');
    ctx.body='登陆页';
  })


// 配置路由
app
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(3002);

// npm install --save art-template
// npm install --save koa-art-template

const Koa = require('koa');
const Router = require('koa-router');
const render = require('koa-art-template');


const app = new Koa();
const router = new Router();
const path = require('path');

//配置koa-art-template模板引擎
render(app, {
    root: path.join('./views'),  //视图位置
    extname: '.html',                    //后缀名
    debug: process.env.NODE_ENV !== 'production'    //是否开启调试模式
  });

router.get('/',async(ctx)=>{
    //ctx.body='首页';
    let list = {
        name:'张三',
        h:'<h2>h2标签</h2>'
    }
    await ctx.render('index',{
        list:list
    });
})

router.get('/news',async(ctx)=>{
    ctx.body='新闻页';
})


app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
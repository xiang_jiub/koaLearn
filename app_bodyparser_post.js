
//npm install --save koa-bodyparser
// 引入 var bodyParser = require('koa-bodyparser')
// app.use(bodyParser());
// ctx.request.body;
const Koa = require('koa');
const Router = require('koa-router');
const Views = require('koa-views');
var bodyParser = require('koa-bodyparser')

const app = new Koa();
const router = new Router();



app.use(Views('views',{
    extension:'ejs'
}))

router.get('/',async(ctx)=>{
    await ctx.render('form')
})

// router.get('/news',async(ctx)=>{
    
// })
app.use(bodyParser()); //配置post中间件
router.post('/doAdd',async(ctx)=>{
    //获取表单提交的数据
    ctx.body=ctx.request.body;
})

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
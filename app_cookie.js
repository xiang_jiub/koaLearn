
// cookie 保存在浏览器客户端
// 可以让我们用同一个浏览器访问同一个域 名的时候共享数据
// 保存用户信息
// 浏览器历史记录
// 猜你细化的功能
// 免登陆
// 多个页面之间的数据传值
const Koa = require('koa');
const Router = require('koa-router');
const render = require('koa-art-template');


const app = new Koa();
const router = new Router();
const path = require('path');

//配置koa-art-template模板引擎
render(app, {
    root: path.join('./views'),  //视图位置
    extname: '.html',                    //后缀名
    debug: process.env.NODE_ENV !== 'production'    //是否开启调试模式
  });

router.get('/',async(ctx)=>{
   
    // koa中没法直接设置中文的cookie
    ctx.cookies.set('userinfo', 'zhangsan', {
        // domain: 'localhost',  // 写cookie所在的域名  正常情况不用设置
        // path: '/index',       // 写cookie所在的路径  配置访问的页面
        maxAge: 10 * 60 * 1000, // cookie有效时长
        // expires: new Date('2017-02-15'),  // cookie失效时间
        // httpOnly: false,  // 是否只用于http请求中获取 true 表示cookie只在服务端可用  false客户端可用
        // overwrite: false  // 是否允许重写
      })
    let list = {
        name:'张三'
    }
    await ctx.render('index',{
        list:list
    });
})

router.get('/news',async(ctx)=>{
    var userinfo = ctx.cookies.get('userinfo');
    console.log(userinfo);
    let app={
        name:'张三最大'
    }
    await ctx.render('news',{
        list:app
    })
})


app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
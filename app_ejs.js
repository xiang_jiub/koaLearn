// npm install koa-views --save


// npm install ejs --save


const Koa = require('koa');
const Router = require('koa-router');
var views = require('koa-views');

// 实例化koa
const app = new Koa();
const router = new Router();

// 配置模板引擎中间件 --第三方中间件
// app.use(views('views', {
//     map: {
//       html: 'ejs'
//     }
//   }));//应用ejs模板引擎 文件为html


app.use(views('views',{
    extension:'ejs'  //文件为ejs
}))

app.use(async(ctx,next)=>{
    ctx.state.userinfo='张三'; //中间件配置的公共信息
    await next();
})

router.get('/', async (ctx) => {
    let title="nihao"
    await ctx.render('index',{
        title:title // 绑定数据
    });
})

router.get('/news',async(ctx)=>{
    let arr = ['111','222','33'];
    await ctx.render('news',{
        list:arr
    })
})

// 配置路由
app
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(3000);
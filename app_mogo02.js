
//引入db数据

const Koa = require('koa');
const Router = require('koa-router');
const render = require('koa-art-template');
const path = require('path');
const DB =require('./module/db');
const app = new Koa();
const router = new Router();


//配置koa-art-template模板引擎
render(app, {
    root: path.join('./views'),  //视图位置
    extname: '.html',                    //后缀名
    debug: process.env.NODE_ENV !== 'production'    //是否开启调试模式
  });

router.get('/',async(ctx)=>{
    //console.time('start');
    var result = await DB.find('news',{});
    // console.timeEnd('start');

    // console.log(result);
    // let list = {
    //     name:'张三',
    //     h:'<h2>h2标签</h2>'
    // }
    await ctx.render('index',{
        list:result
    });
})
//新增
router.get('/add',async(ctx)=>{
    let data = await DB.insert('news',{"username":"可怜的向哥","age":28,"sex":"男","staus":"1"});
    console.log(data.result);
    ctx.body="新增页面";
})
//编辑
router.get('/edit',async(ctx)=>{
    let data = await DB.update('news',{"username":"lisi"},{"username":"李四"});
    console.log(data.result);
    ctx.body="编辑页面";
})
//删除
router.get('/delete',async(ctx)=>{
    let data = await DB.remove('news',{"username":"李四"});
    console.log(data.result);
    ctx.body="删除页面";
})

router.get('/news',async(ctx)=>{
    ctx.body='新闻页';
})


app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);

const Koa = require('koa');
const Router = require('koa-router');
const Views = require('koa-views');
var common = require('./module/common.js');

const app = new Koa();
const router = new Router();

app.use(Views('views',{
    extension:'ejs'
}))

router.get('/',async(ctx)=>{
    await ctx.render('form')
})

// router.get('/news',async(ctx)=>{
    
// })

router.post('/doAdd',async(ctx)=>{
    //获取表单提交的数据 原生node获取post提交的数据
    var data= await common.getpostData(ctx);
    console.log(data);
    ctx.body=data;
})

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
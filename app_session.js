
// cookie 保存在浏览器客户端
// 可以让我们用同一个浏览器访问同一个域 名的时候共享数据
// 保存用户信息
// 浏览器历史记录
// 猜你细化的功能
// 免登陆
// 多个页面之间的数据传值


const Koa = require('koa');
const Router = require('koa-router');
const render = require('koa-art-template');
const session = require('koa-session');
const path = require('path');

const app = new Koa();
const router = new Router();


//配置koa-art-template模板引擎
render(app, {
    root: path.join('./views'),  //视图位置
    extname: '.html',                    //后缀名
    debug: process.env.NODE_ENV !== 'production'    //是否开启调试模式
  });
// 配置session
app.keys = ['some secret hurr'];
const CONFIG = {
  key: 'koa.sess', /** (string) cookie key (default is koa.sess) */
  maxAge: 86400000,// 过期时间
  autoCommit: true, /** (boolean) automatically commit headers (default true) */
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false,//快要过期的时候设置 /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
  //secure: true, /** (boolean) secure cookie*/  //加密
  sameSite: null, /** (string) session cookie sameSite options (default null, don't set it) */
};

app.use(session(CONFIG, app));

router.get('/',async(ctx)=>{
    //设置session
    ctx.session.sexname='老李';
   
   
    let list = {
        name:'张三'
    }
    await ctx.render('index',{
        list:list
    });
})

router.get('/news',async(ctx)=>{
    console.log(ctx.session.sexname);
    ctx.body='登录成功';
})


app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
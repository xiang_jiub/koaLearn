// koa-static 静态资源中间件 静态web服务

// npm install koa-static --save

// const Static = require('koa-static')

// app.use(Static('static'))

const Koa = require('koa');
const Router = require('koa-router');
const Views = require('koa-views');
const static = require('koa-static');
const bodyParser = require('koa-bodyparser')

const app = new Koa();
const router = new Router();




app.use(Views('views',{
    extension:'ejs'
}));
//首先去conent目录去找 能找到返回文件 找不到next()
app.use(static('./conent'));  //可以配置多个

router.get('/',async(ctx)=>{
    await ctx.render('form')
})

// router.get('/news',async(ctx)=>{
    
// })

app.use(bodyParser()); //配置post中间件
router.post('/doAdd',async(ctx)=>{
    //获取表单提交的数据
    ctx.body=ctx.request.body;
})

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);
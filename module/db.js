

var MongeoClient = require('mongodb').MongoClient;
var  Config = require('./config.js');

class Db{
    constructor(){
        this.dbClient = '';
        this.connect(); //实例化的时候就链接数据库
    }
    //单例
    static getInstance(){
        if(!Db.instance){
            Db.instance = new Db();
        }
        return Db.instance;
    }

    //链接数据库
    connect(){
        let _that = this;
        return new Promise((reslove,reject)=>{
            if(!_that.dbClient){
                MongeoClient.connect(Config.dbUrl,(err,client)=>{
                    if(err){
                        console.log(err);
                        reject(err);
                    }else{
                        _that.dbClient = client.db(Config.dbName);
                        reslove(_that.dbClient);
                    }
                })
            }else{
                reslove(_that.dbClient);
            }
            
        })
      
    }
    

    //查询数据
    find(collectionName,json){
        return new Promise((reslove,reject)=>{
            this.connect().then((db)=>{
                var result = db.collection(collectionName).find(json);
                result.toArray(function(err,docs){
                    if(err){
                        reject(err);
                        result;
                    }
                    reslove(docs);
                })
            })
        })
        
    }

    update(collectionName,json1,json2){
        return new Promise((reslove,reject)=>{
            this.connect().then((db)=>{
                db.collection(collectionName).updateOne(json1,{
                    $set:json2
                },(err,result)=>{
                    if(err){
                        reject(err);
                    }else{
                        reslove(result);
                    }
                })
            })
        })
    }

    insert(collectionName,json){
        return new Promise((reslove,reject)=>{
            this.connect().then((db)=>{
                db.collection(collectionName).insertOne(json,function(err,result){
                    if(err){
                        reject(err);
                    }else{
                        reslove(result);
                    }
                })
            })
        })
    }

    remove(collectionName,json){
        return new Promise((reslove,reject)=>{
            this.connect().then((db)=>{
                db.collection(collectionName).removeOne(json,function(err,result){
                    if(err){
                        reject(err);
                    }else{
                        reslove(result);
                    }
                })
            })
        })
    }
}


module.exports = Db.getInstance();
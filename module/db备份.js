

var MongeoClient = require('mongodb').MongoClient;
var  Config = require('./config.js');

class Db{
    constructor(){
        this.dbClient = '';
        this.connect();
    }
    //单例
    static getInstance(){
        if(!Db.instance){
            Db.instance = new Db();
        }
        return Db.instance;
    }

    //链接数据库
    connect(){
        let _that = this;
        return new Promise((reslove,reject)=>{
            if(!_that.dbClient){
                MongeoClient.connect(Config.dbUrl,(err,client)=>{
                    if(err){
                        console.log(err);
                        reject(err);
                    }else{
                        _that.dbClient = client.db(Config.dbName);
                        reslove(_that.dbClient);
                    }
                })
            }else{
                reslove(_that.dbClient);
            }
            
        })
      
    }
    update(){

    }

    //查询数据
    find(collectionName,json){
        return new Promise((reslove,reject)=>{
            this.connect().then((db)=>{
                var result = db.collection(collectionName).find(json);
                result.toArray(function(err,docs){
                    if(err){
                        reject(err);
                        result;
                    }
                    reslove(docs);
                })
            })
        })
        
    }
}


var myDb = Db.getInstance();
myDb.find('news',{}).then(function(data){
    console.log(data);
});
